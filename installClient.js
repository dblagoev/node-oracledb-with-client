const { execSync } = require('child_process');
const fs = require('fs');
const path = require('path');

const targetFolder = path.dirname(require.resolve('oracledb/README.md'));

execSync('wget -q https://download.oracle.com/otn_software/linux/instantclient/instantclient-basiclite-linuxx64.zip -O ./oracle-client.zip');

console.log('Downloaded the Oracle SQL Server basic lite instant client (Linux x64)');

execSync(`unzip -o ./oracle-client.zip -d ${path.join(targetFolder, 'build/Release/')}`);

console.log('Extracted the client');

fs.unlinkSync('./oracle-client.zip');

const initHostFile = require.resolve('request-promise-native/lib/rp.js');

fs.appendFileSync(initHostFile, `
require('oracledb-with-client');
`);
