const { execSync } = require('child_process');
const fs = require('fs');
const path = require('path');

if (!process.env.workerProcessData) {
    const clientReleaseFolder = path.join(process.cwd(), '/node_modules/oracledb/build/Release/');
    const availableClients = fs.readdirSync(clientReleaseFolder, { withFileTypes: true }).filter(i => i.isDirectory()).map(i => i.name);
    const selectedClient = availableClients[0];
    const clientFolder = path.join(clientReleaseFolder, selectedClient);

    if (selectedClient) {
        if (!fs.existsSync('/usr/lib/instantclient')) {
            execSync(`
                ln -s ${clientFolder} /usr/lib/instantclient
                ln -s /usr/lib/instantclient/libclntsh.so.19.1 /usr/lib/libclntsh.so &&
                ln -s /usr/lib/instantclient/libocci.so.19.1 /usr/lib/libocci.so &&
                ln -s /usr/lib/instantclient/libociicus.so /usr/lib/libociicus.so &&
                ln -s /usr/lib/instantclient/libnnz19.so /usr/lib/libnnz19.so &&
                ln -s /usr/lib/libnsl.so.2 /usr/lib/libnsl.so.1 &&
                ln -s /lib/libc.so.6 /usr/lib/libresolv.so.2 &&
                ln -s /lib64/ld-linux-x86-64.so.2 /usr/lib/ld-linux-x86-64.so.2
            `);
        }

        process.env.ORACLE_HOME = process.env.ORACLE_BASE = process.env.TNS_ADMIN = '/usr/lib/instantclient';
        process.env.LD_LIBRARY_PATH = process.env.ORACLE_HOME + (process.env.LD_LIBRARY_PATH ? ':' + process.env.LD_LIBRARY_PATH : '');
    }
}
